# Getting backup from your postgres database

Hi. you can dump your postgres database with this script. these backup files stored in ```.sql``` file and then, that compress to ```.tar.xz``` file and then, it will encrypt by your passphare which you give at script .

For getting backup from your postgres database, you must run this code:
```
python3 backup_sql.py backup --psql_user=PSQL-USERNAME --psql_dbname=PSQL-DATABASE-NAME --backup_path=PATH-BACKUPS --passphare=PASSPHARE-ENCRYPTION --receiver_email=EMAIL'S-RECEIVER-ENCRYPTION [optional]--psql_password=PASSWORD-USER
```

your backup stored in ```your-backup-path/backups/```.

you can set crontab for getting backup from your database to daily or weekly or etc.
for daily backup, use this mode: ```0 0 * * *```. this mode can run this code every day in ```00:00``` o'clock.

for set cron, use this:
```
crontab -e
0 0 * * * python3 /path-of-file/backup_sql.py backup "and set keywords"
```