import os
import datetime
import subprocess
import logging
import tarfile
import argparse
import gnupg
import sqlite3
import re

class GetBackupSqlite:
    def __init__(self, sql_db_path, backup_path, passphare, receiver_email):
        self.sql_db_path = sql_db_path
        self.backup_path = backup_path
        self.passphare = passphare
        self.enc_email_receiver = receiver_email
        self.backups_path = self.backup_path + 'backups/'
        self.logfile = self.backup_path + 'sqlite_bak.log'
        self.compress_backup_format = '{}.tar.xz'
        logging.basicConfig(
            filename=self.logfile,
            level=logging.NOTSET,
            format='[%(asctime)s] : %(message)s',
        )

    def tarfile_today_absolute_path(self):
        return self.get_path_of_date() + '/' + self.compress_backup_format.format(self.get_date())

    def sql_dump_today_absolute_path(self):
        return self.get_path_of_date() + '/' + str(self.get_date()) + '.sql'

    def backup_path_is_exist(self):
        return os.path.exists(self.backup_path)

    def sqlite_exist(self):
        sqlite = subprocess.run(
            ['which', 'sqlite'], 
        )
        if sqlite.returncode == 1:
            return False
        return True

    def get_date(self,):
        return datetime.datetime.now().date()

    def get_path_of_date(self):
        return f'{self.backups_path}{self.get_date()}'

    def make_date_directory(self):
        os.makedirs(self.get_path_of_date())

    def run_backup_code(self):
        con = sqlite3.connect(self.sql_db_path)
        with open(self.sql_dump_today_absolute_path(), 'w') as f:
            for line in con.iterdump():
                f.write(f'{line}\n')
        con.close()

    def compress_backup(self):
        with tarfile.open(self.tarfile_today_absolute_path(), 'w:xz') as _tarfile:
            sql_backup = self.get_path_of_date() + f'/{self.get_date()}' + '.sql'
            _tarfile.add(sql_backup, arcname=str(self.get_date()) + '.sql')

    def remove_trash_files(self):
        os.remove(self.sql_dump_today_absolute_path())
        os.remove(self.tarfile_today_absolute_path())

    def gnupg_exist(self):
        _exist = subprocess.run(
            ['which', 'gpg'],
            capture_output=True
        )
        return _exist.stdout

    def encrypt_backup(self, gpg_path):
        validated_gpg_path = gpg_path.decode('ASCII').strip('\n')
        gpg = gnupg.GPG(gpgbinary=validated_gpg_path)
        with open(self.tarfile_today_absolute_path(), 'rb') as _file:
            encrypted_file = gpg.encrypt_file(
                _file,
                recipients=[self.enc_email_receiver],
                passphrase=self.passphare,
                symmetric=True,
                output=self.tarfile_today_absolute_path() + ".gpg",
            )
        logging.info('ok : ' + str(encrypted_file.ok))
        logging.info('encrypt status : ' + str(encrypted_file.status))

    def get_backup(self):
        print('getting backup from sqlite started!')
        logging.info('getting backup from sqlite started!')

        if not self.backup_path_is_exist():
            print(self.backup_path_is_exist())
            raise ValueError('Backup path does not exist.')
        if not self.sqlite_exist():
            raise SystemError('sqlite not installed!')

        _gnupg = self.gnupg_exist()
        if not bool(_gnupg):
            raise SystemError('gnupg (gpg) not installed!')

        if os.path.exists(self.get_path_of_date()):
            backup_today_path = os.listdir(self.get_path_of_date())
            if len(backup_today_path) == 1 and \
                os.path.isfile(self.get_path_of_date() + '/' + backup_today_path[0]) and backup_today_path[0].endswith('.tar.xz'):
                print('backup for today is exist!')
                logging.info('backup for today is exist!')
                return
        else:
            self.make_date_directory()

        a = self.run_backup_code()

        self.compress_backup()
        self.encrypt_backup(_gnupg)

        self.remove_trash_files()

        logging.info('Getting backup successfully finished!')
        print('Getting backup successfully finished!')

def email(email):
    re_pattern = r'[\d\w]+@[\d\w]+\.+[\d\w]+[^*]'
    if re.fullmatch(re_pattern, email):
        return email
    raise ValueError('There is not email not matched!')

parser = argparse.ArgumentParser()
sub_parser = parser.add_subparsers(help='Getting backup', required=True)
backup_parser = sub_parser.add_parser('backup', help='Getting backup from sqlite database')
backup_parser.add_argument('--sqlite_path', required=True, help='sqlite database path.')
backup_parser.add_argument('--backup_path', required=True, help='the directory which backup files will save inside it.')
backup_parser.add_argument('--passphare_encrypt', required=True, help='passphare for encrypt backup file.')
backup_parser.add_argument('--email_receiver', required=True, type=email, help='email\'s receiver for encrypt backup file.')

if __name__ == '__main__':
    arguments = parser.parse_args()
    kw = {
        'sql_db_path': arguments.sqlite_path,
        'backup_path': arguments.backup_path,
        'passphare': arguments.passphare_encrypt,
        'receiver_email': arguments.email_receiver
    }

    GetBackupSqlite(**kw).get_backup()