import os
import time
import datetime
import json
import logging
import tarfile
import shutil
import threading
from progress.spinner import Spinner
import argparse


class BackupRestore:
    def __init__(self, default_backup_path, data_dir):
        # Default backup path is path which backups file, log file and etc generated inside that
        # self.default_bak_path = default_backup_path if default_backup_path.endswith('/') else default_backup_path + '/'
        self.default_bak_path = self.get_absolute_path(default_backup_path) if default_backup_path else default_backup_path
        self.logfile = self.default_bak_path + 'backup.log'
        self.backup_saved_directory = self.default_bak_path + 'backups/'
        self.backup_name = '{}.tar.xz'
        self.mapfile_name = 'map.json'
        # Data directory is a path which backup get from that path
        self.data_directory = self.get_absolute_path(data_dir) if data_dir else data_dir
        self.backup_working_directory = self.default_bak_path + 'in_working/'
        self.get_backup_from_this_dir = self.backup_working_directory + 'backup'
        self.data_file = self.default_bak_path + 'data.json'
        self.datadir_key = 'datadir'
        self.backupdir_key = 'backupdir'

        logging.basicConfig(
            filename=self.logfile,
            level=logging.NOTSET,
            format='[%(asctime)s] : %(message)s',
        )
    def get_absolute_path(self, path):
        # Get absolute path.
        return os.path.abspath(path) + '/'

    def file_exist(self, file):
        # is any file existed or not
        listdir = os.listdir(self.backup_saved_directory)
        if len(listdir) == 0:
            return False
        listdir = self.sort_date_object_list(listdir)
        map_is_exist = os.path.isfile(
            self.backup_saved_directory + str(listdir[0].date()) + '/' + file
        )
        return map_is_exist

    def load_data_map(self, _dir):
        # Load map file data
        # Map file data is json list which get from input directory 
        file = open(_dir + self.mapfile_name, 'r')
        data = json.load(file)
        file.close()
        return data

    def sort_date_object_list(self, datalist):
        # Backup files store in 'self.backup_saved_directory' and inside that, many directory exist which that name is by date of generated backup.
        # This method return a list have sorted directory by date
        sorted_list_by_date = []
        for i in datalist:
            sorted_list_by_date.append(
                datetime.datetime.strptime(i, "%Y-%m-%d")
            )
        sorted_list_by_date.sort()
        return sorted_list_by_date
    
    def get_path_all_files(self, basic_path):
        # This is get path of all files 
        _path_files = []
        if basic_path == '.':
            basic_path = './'
        for i in os.listdir(basic_path):
            real_path = basic_path + i + '/'
            if os.path.isdir(real_path):
                a = self.get_path_all_files(real_path)
                _path_files.extend(a)
            else:
                _path_files.append(real_path[:-1])
        return _path_files

    
class GetBackup(BackupRestore):
    def __init__(self, default_backup_path, data_dir):
        super().__init__(default_backup_path, data_dir)
        self.make_backup_stored_directory()

    def make_backup_stored_directory(self):
        if not os.path.exists(self.backup_saved_directory):
            os.makedirs(self.backup_saved_directory)

    def get_date(self):
        return datetime.datetime.now().date()

    def generate_data_file(self):
        jsondata = {
            self.datadir_key: self.data_directory,
            self.backupdir_key: self.backup_saved_directory
        }
        
        data = self.serialize_data(jsondata)
        with open(self.data_file, 'w') as file:
            file.write(data)
        logging.info('Datafile generated.')

    def serialize_data(self, data):
        # Serialize data for write to json file
        return json.dumps(data)

    def generate_mapfile(self, data, path):
        with open(path + self.mapfile_name, 'w') as file:
            file.write(data)
        logging.info('Map file successfully generated!')

    def load_all_data_map(self):
        # Merge All data-map-file for get all information from pathes
        # This is useful for optimzed get backup from media files
        # In this case, all time we have updated data because in this method deleted file are validated
        # and final output always have updated data.
        # For Exmaple, user may uploaded a file with 'exmaple.png' name and this code get a backup file from media which 'exmaple.png' inside that.
        # also when backup generated, 'example.png' existed in mapfile.json with that backup.
        # after one day, example.png delete by user. Then this method delete example.png from all datamap.
        # and if user add again example.png, it is considered a new file and there is no conflict between this example.png and previous example.png
        # I will use this alogorithm in restore bakup
        backup_dir_list = os.listdir(self.backup_saved_directory)
        sorted_dir_list = self.sort_date_object_list(backup_dir_list)
        # Sorted diretory list, return a list which all data is datetime object.
        # Then if we need use data of this list, we should convert datetime object to string.
        all_files = []
        deleted_files = []
        for i in sorted_dir_list:
            loaded_data = self.load_data_map(self.backup_saved_directory + str(i.date()) + '/')
            # First remove deleted files from all data
            deleted_files.extend(loaded_data.get('deleted'))
            if deleted_files and all_files:
                all_files = list(set(all_files) - set(deleted_files))
                deleted_files = []
            # Then add newed files
            all_files.extend(loaded_data.get('newed'))
        return all_files

    def newed_files_pathes(self):
        # Get new Files pathes
        all_data_map_saved = self.load_all_data_map()
        all_data_map = self.get_path_all_files(self.data_directory)
        return list(
            set(all_data_map) - set(all_data_map_saved)
        )

    def deleted_files_pathes(self):
        # Get Deleted Files
        if self.file_exist(self.mapfile_name):
            all_data_map_saved = self.load_all_data_map()
            all_data_map = self.get_path_all_files(self.data_directory)
            return list(
                set(all_data_map_saved) - set(all_data_map)
            )
        else:
            return []

    def generate_tar_xz_file(self, path):
        # Compress directory 
        with tarfile.open(self.backup_working_directory + self.backup_name.format(self.get_date()), 'w:xz') as _tar:
            _tar.add(path)
        logging.info('get backup file successfully!')
        return self.backup_working_directory + self.backup_name.format(self.get_date())

    def make_backup_working_directory(self):
        # Make backup working directory
        # This is for copy all new files and get backup from that
        if not os.path.exists(self.backup_working_directory + 'backup/'):
            os.makedirs(self.backup_working_directory + 'backup/')

    def rm_backup_working_directory(self):
        # Remove backup working directory when backup getted
        shutil.rmtree(self.backup_working_directory)

    def copy_files_for_backup(self, pathlist):
        # Copy files to working directory for get backup from that
        for i in pathlist:         
            path_without_data_directory = '/' + i.split(self.data_directory)[1]
            _file = self.get_backup_from_this_dir + path_without_data_directory
            path_for_copy_file = self.get_backup_from_this_dir + '/'.join(path_without_data_directory.split('/')[:-1]) + '/'
            if not os.path.exists(path_for_copy_file):
                os.makedirs(path_for_copy_file)
            shutil.copyfile(i, _file)

    def backup_today_exist(self):
        backup_exist = os.path.exists(
            self.backup_saved_directory + str(self.get_date()) + self.backup_name.format(self.get_date())
        )
        return backup_exist

    def get_backup(self):
        """
        Get Backup from Input directory
        """
        start_time = time.time()

        if self.backup_today_exist():
            print('A backupfile for today exist!')
            logging.info('A backupfile for today exist!')
            return

        print('Getting backup started!')
        logging.info('Getting backup started.')
        
        self.make_backup_working_directory()

        if self.file_exist(self.mapfile_name):
            new_files = self.newed_files_pathes()
        else:
            new_files = self.get_path_all_files(self.data_directory)
        deleted_files = self.deleted_files_pathes()

        if len(new_files) == 0 and len(deleted_files) == 0:
            self.rm_backup_working_directory()
            logging.info('New or deleted files not detected!')
            print('New or deleted files not detected!')
            return

        today_directory = self.backup_saved_directory + str(self.get_date()) + '/'
        os.mkdir(today_directory)

        if new_files:
            logging.info('Collecting data')
            spinner = Spinner('Collecting data ')
            _copy_threading = threading.Thread(target=self.copy_files_for_backup, args=(new_files,))
            _copy_threading.start()

            while _copy_threading.is_alive():
                spinner.next()
                time.sleep(0.06)

            _copy_threading.join()
            spinner.finish()
            print('Collecting data finished!')

            spinner_tarfile = Spinner('Compressing ')
            backup_dir = self.get_backup_from_this_dir + '/'
            _generate_tarxz_threading = ThreadingWithData(target=self.generate_tar_xz_file, args=[backup_dir,])
            _generate_tarxz_threading.start()

            while _generate_tarxz_threading.is_alive():
                spinner_tarfile.next()
                time.sleep(0.06)

            _generate_tarxz_threading.join()
            spinner_tarfile.finish()
            print('Compressing finished!')

            backup_file = _generate_tarxz_threading.returned_value
            shutil.move(backup_file, today_directory)

        map_data = {'newed': new_files, 'deleted': deleted_files}
        self.generate_mapfile(
            self.serialize_data(map_data),
            today_directory
        )

        self.rm_backup_working_directory()

        # Data file have data_directory path and path of saved backup.
        # This file very useful when we try extract backup.
        datafile_exist = os.path.exists(self.data_file)
        if not datafile_exist:
            self.generate_data_file()

        logging.info(f'Backup file successfully generated to "{today_directory}"')
        logging.info(f'Backup file generated in {time.time() - start_time}')
        print('backup successfully generated!')


class RestoreBackup(BackupRestore):
    """
    This class restore all backup files.
    Algorithm used in this class, can good sync backup data without lost anything's.
    """
    def __init__(self, default_backup_path, data_dir=None, restore_in=None):
        super().__init__(default_backup_path, data_dir)
        self.restore_data_from = self.backup_saved_directory
        self.restore_data_path = self.default_bak_path + 'restore/'
        self.restore_working_directory = self.default_bak_path + 'restore/' + 'in_working/'
        self.restore_in = self.get_absolute_path(restore_in) if restore_in else restore_in 
        datafile = self.load_datafile()
        self.data_directory = datafile.get(self.datadir_key)

    def set_data_dir(self):
        # Set self.data_directory by getting that from data file.
        file = open(self.data_file, 'r')
        data = json.load(file)
        self.data_directory = data.get(self.datadir_key)

    def extract_backup(self, backup_path, restore_path):
        _tarfile = tarfile.open(backup_path)
        _tarfile.extractall(restore_path)
        _tarfile.close()

    def make_restore_path(self):
        # This make restore path for doing task.
        _rwd = os.path.exists(self.restore_working_directory)
        if not _rwd:
            os.makedirs(self.restore_working_directory)

    def rm_restore_path(self):
        # Remove restore working directory
        shutil.rmtree(self.restore_data_path)

    def move_data(self, _from, _to):
        # Move file
        try:
            shutil.move(_from, _to)
        except shutil.Error as _error:
            logging.warning(_error)
            print(f'"{_from}" can not move. Error message:\n {_error}')

    def load_datafile(self):
        file = open(self.data_file, 'r')
        data = json.load(file)
        return data


    def restore_backup(self):
        #Restore all backup files.
        start_time = time.time()

        print('Restore backup started!')
        logging.info('Restore backup started.')

        backup_list = os.listdir(self.backup_saved_directory)
        sorted_dir = self.sort_date_object_list(backup_list)

        if not self.file_exist(self.mapfile_name) or not self.file_exist(self.backup_name.format(sorted_dir[0].date())):
            logging.info('Mapfile or backup file not found!')
            print('Mapfile or backup file not found!')
            return 

        self.make_restore_path()

        new_files_datamap_exist = False
        for _path in sorted_dir:            
            restore_path = self.restore_working_directory + str(_path.date()) + '/'
            restore_path_exist = os.path.exists(restore_path)
            if not restore_path_exist:
                os.makedirs(restore_path)
            
            backup_path = self.backup_saved_directory + str(_path.date()) + '/'
            path_backup_file = backup_path + self.backup_name.format(_path.date())
            backup_exist = os.path.exists(backup_path + self.backup_name.format(_path.date()))
            # If any backup file does not exist, this meaning may new files not exist and deleted files only existed.
            # In this mode we ignore backup file and only use mapfile.
            if backup_exist:
                print('Extract backup file started.')
                logging.info('Extract backup file started.')

                spinner = Spinner(f'Extracting backup {path_backup_file} ')
                _thread = threading.Thread(target=self.extract_backup, args=[path_backup_file, restore_path])
                _thread.start()
                while _thread.is_alive():
                    # This loop isnt running
                    spinner.next(1)
                    time.sleep(0.06)
                _thread.join()
                spinner.finish()

                logging.info(f'backup of {_path.date()} is extracted!')
                print(f'backup of {_path.date()} is extracted!')

            data_mapfile = self.load_data_map(backup_path)
            deleted_files = data_mapfile.get('deleted')
            # 'new_files_datamap_exist' is a variable that help to recognize this path is first backupfile or not 
            # When loop started for first time, this condition will be skipped and will be run in next time.
            # If any deleted file exist and 'new_files_datamap_exist' is exist, then that removed "deleted files".
            if deleted_files and new_files_datamap_exist:
                for i in deleted_files:
                    try:
                        os.remove(i)
                    except:
                        pass
            
            newed_files = data_mapfile.get('newed')
            if newed_files:
                for newedfile in newed_files:
                    # Path of newedfile saved in mapfile, like as this path : "self.data_directory"/example.png
                    # But path of extracted file like as this path : "self.get_backup_from_this_dir/example.png"
                    # These variable have difference and are not the same
                    # in bottom we replace 'self.data_directory' to 'self.get_backup_from_this_dir' for right get path of extracted file. 
                    validated_path = newedfile.split(self.data_directory)
                    path_of_extracted_file = restore_path + self.get_backup_from_this_dir + '/' + validated_path[1]
                    
                    if self.restore_in:
                        newedfile = self.restore_in + validated_path[1]
                        
                    # Check if parents directory file not exist, make that. 
                    path_newedfile_without_filename = '/'.join(newedfile.split('/')[:-1]) + '/'
                    if not os.path.exists(path_newedfile_without_filename):
                        os.makedirs(path_newedfile_without_filename)

                    # and Then move extracted file to path of received from mapfile.
                    self.move_data(path_of_extracted_file, newedfile)

                    # Check top of this condition in lines up above : 'if deleted_files and new_files_datamap_exist'
                    new_files_datamap_exist = True
            print(f'Restore backup of {_path.date()} finished!')
            logging.info(f'Restore backup of {_path.date()} finished!')
        
        self.rm_restore_path()
        logging.info(f'Backup restored in {time.time() - start_time}s!')
        logging.info(f'Backup restored in "{self.restore_in if self.restore_in else self.data_directory}"')
        logging.info('Restore all backup completed.')

        print('Backup restored!')
        print(f'you can see restored backup in {self.restore_in if self.restore_in else self.data_directory}')


class ThreadingWithData(threading.Thread):
    """
    This class can be return back value of a function which be thread.
    """
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, *, daemon=None):
        super().__init__(group, target, name, args, kwargs, daemon=daemon)
        self.returned_value = None

    def run(self):
        _data = self._target(*self._args)
        self.returned_value = _data





# Parser for getting arguments when file is running
parser = argparse.ArgumentParser()
# first we need a subparser for parsing 2 arguments
sub_parser = parser.add_subparsers(help='Backup and Restore', required=True, dest='mode')
# 'backup' and 'restore' are argument we need.
# So we add 2 parser for 'backup' and 'restore'
backup_parser = sub_parser.add_parser('backup', help='Getting backup from input directory "--datadir" to "--backupdir".')
restore_parser = sub_parser.add_parser('restore', help='Restoring backup files from input directory.')
# For each parser we can add many sub argument to it.
# These arguments can be optional or positional.
backup_parser.add_argument('--datadir', required=True, help='The directory which you need to get backup from that.')
backup_parser.add_argument('--backupdir', required=True, help='The directory which backup files will save inside it.')

restore_parser.add_argument('--restorepath', required=False, help='The directory backupfile resotre inside it.')
restore_parser.add_argument('--backupdir', required=True, help='The directory which backup files saved inside that.')

if __name__ == '__main__':
    parse_args = parser.parse_args()
    
    if parse_args.mode == 'backup':
        if parse_args.datadir and parse_args.backupdir:
            GetBackup(default_backup_path=parse_args.backupdir, data_dir=parse_args.datadir).get_backup()
    elif parse_args.mode == 'restore':
        if parse_args.backupdir and parse_args.restorepath:
            RestoreBackup(default_backup_path=parse_args.backupdir, restore_in=parse_args.restorepath).restore_backup()
        elif parse_args.backupdir:
            RestoreBackup(default_backup_path=parse_args.backupdir).restore_backup()